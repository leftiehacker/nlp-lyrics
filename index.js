const nlp = require('compromise')
const Musix = require('musixmatch-node')

// MusixMatch API Key required
require('dotenv').config()
const mxm = new Musix(process.env.API_KEY)

// List of songs to analyze
const songMatcher = [{
  q_track: '&run',
  q_artist: 'sir sly',
},{
  q_track: 'san francisco',
  q_artist: 'the mowgli\'s',
},{
  q_track: 'oh devil',
  q_artist: 'electric guest',
},{
  q_track: 'you\'re on',
  q_artist: 'madeon',
},{
  q_track: 'adderall',
  q_artist: 'max frost',
},{
  q_track: 'paranoia',
  q_artist: 'max frost',
},{
  q_track: 'white lies',
  q_artist: 'max frost',
}]

const lyrics = async (song) => {
  const { message } = await mxm.getLyricsMatcher(song)
  return message.body.lyrics.lyrics_body
}


(async () => {
  // Get song lyrics
  const songs = await Promise.all(songMatcher.map(async (song) => await lyrics(song)))

  // Find frequency of most used noun(s) in lyrics
  const freq = songs.map(s => nlp(s).nouns().out('frequency'))

  // Print data
  freq.forEach((f, i) => { 
    const a = f[0]
    const output = a
      ? `${a.normal}, ${a.count}, ${a.percent}%`
      : `-- not found --`
    
    console.log(`${i + 1}. ${output}`)
  })
})()
